const path = require('path');

module.exports = {
  //...
  devServer: {
    clientLogLevel: 'info',
    contentBase: path.join(__dirname, 'public/'),
    
    https: true
    // compress: true,
    // port: 9000,
  },
};